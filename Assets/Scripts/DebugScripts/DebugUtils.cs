#define DEBUG
 
using System;
using System.Diagnostics;
// http://forum.unity3d.com/threads/144297-System-Diagnostics-Debug-Log-error
using Debug = UnityEngine.Debug;

public class DebugUtils
{
    [Conditional("DEBUG")]
    public static void Assert(bool condition)
    {
		Debug.Log("Asserting");
        if (!condition) throw new Exception();
    }
	
}

