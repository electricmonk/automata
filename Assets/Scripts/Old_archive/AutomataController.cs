using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AutomataController : MonoBehaviour {
	
	// make this an array later
	// multiple arrays that co-eist on the location square and possibly interact with other systems
	
	public Transform cellArrayPrefab; 
	
	// the cell array being created
	private AutomataCellArray cellArray; 
	
	// -------- Debug flags ---------
	public bool debug_BuildTestCases = false;
	
	// Use this for initialization
	void Start(){
		
		DebugUtils.Assert(true);
	
		Debug.Log("True Is True");
	//	DebugUtils.Assert(false);
		Debug.Log("False Is Probably False");
		
		CreateCellArray();
	} 
	
	void CreateCellArray(){
		if(debug_BuildTestCases){
	//		yield return StartCoroutine(Debug_BuildTestCases());
	//		yield return new WaitForSeconds(0.0f);
		}
		
		Transform tr;
		GameObject go;
		
		if(cellArrayPrefab == null){
			// xx will crash without a Cell prefab in the AutomataCellArray
			Debug.LogWarning("no cell array prefab, instantiating default array");	
			
			go = new GameObject();
			go.transform.parent = this.transform;
			go.AddComponent<AutomataCellArray>();
			tr = go.transform;
		}else {
			Debug.Log("Instanting New Cell Array");
			tr = (Transform)Instantiate(cellArrayPrefab, transform.position, Quaternion.identity);
			
		}
		
		if(tr == null){
			Debug.LogWarning("No Array Instantiated");	
		}
		
		cellArray = tr.GetComponent<AutomataCellArray>();
		cellArray.transform.parent = this.transform;
		cellArray.InstantiateCellArray();
	}
	
	
	// Update is called once per frame
	void Update () {
		// ~ add delay here	
		if(Input.GetButtonDown("Jump")){
			
			StepUpdate();
		
		}

	}
	
	private void StepUpdate(){
		cellArray.StepUpdate();
	}

	//********************* DEBUG METHODS *********************
	
		
//	// Test case
//	// Build a grid and check for errors
//	IEnumerator Debug_BuildTestCases(){
//	// get ready
//		DestroyCells();
//		
//		Debug.Log("Debug - Build tett case 1");
//		
//	// test case 1x1-----------------------	
//		if(GenerateCells(1,1)){
//			Debug.Log("Debug - Cell Array Generated Successfully: " + 1 + "x" + 1);	
//		};
//		// run additional test cases
//		if(debug_VisualTestCases){
//			yield return StartCoroutine(Debug_VisualScan());	
//		}
//		//clean up
//		yield return new WaitForSeconds(1.0f);
//		DestroyCells();
//		yield return new WaitForSeconds(1.0f);
//			
//		Debug.Log("Debug - Build tett case 2");	
//		
//	// test case 5x4 -----------------------
//		if(GenerateCells(4,5)){
//			Debug.Log("Debug - Cell Array Generated Successfully: " + 4 + "x" + 5);	
//		};
//		
//		yield return StartCoroutine("Debug_BoundaryConditions");
//		
//		// run additional test cases
//		if(debug_VisualTestCases){
//			yield return StartCoroutine(Debug_VisualScan());	
//		}
//		//clean up
//		yield return new WaitForSeconds(1.0f);
//		DestroyCells();
//		
//		yield return new WaitForSeconds(1.0f);
//	}

}
