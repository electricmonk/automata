using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AutomataRule:MonoBehaviour{
	
	// translated in to an int in the cell, maybe standardize
	public enum NumberOfNeighbors{
		fourNeighbors,
		eightneighbors,
		twelveNeighbors
	}
	
	public NumberOfNeighbors neighbors = NumberOfNeighbors.eightneighbors; 
	
	public abstract int CalcNextState(List<Cell> neighborCells, int currentState);
	
	// returns the neighbors code from the enum
	public virtual int GetNeighborType(){
		return (int)neighbors;
	}
}
