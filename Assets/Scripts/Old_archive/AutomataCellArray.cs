using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AutomataCellArray : MonoBehaviour {

	// the image that stores the automata state as pixel color information
	//public Sprite automataPixelGraph;

	// parameters
	// number of cells
	// must be less then pixel size of automataPixel Graph
	public int numberOfRows = 1024;
	public int numberOfColumns = 1024;
	
	// the cell rule to use
	public AutomataRule cellRule;
	
	// the cell prefab used
	public Transform cellPrefab;
	
	// basic random settings
	public bool useRandomCellStates = true; // do we randomize starting state? 
	// % chance state is black(1)
	//  activating 'percentSeedStarting'% of the cells at random
	public float cellsStartedActive = .2f;
	
	// nerdy random settings
	public bool generateRandomSeed = false; // use the same seed until otherwise noted
	public int randomSeed = 12345;
	
	// ?  2d array kept as a 2d array
	private List<Cell> cellArray;
	
	// system flags
	
		// Debug flags
	public bool debug_TestEdgeCases = false; // needs function
	public bool debug_VisualTestCases = false;
	public bool debug_VisualPulse = false;
	
	// ******* debug values *******
	// delay before next Cell is fired
	public float d_iterationTimer = .001f;
	// delay before it returns to the previous value
	public float d_fadeTimer = .1f;
	// delay between test
	// right now si 2 * fadeTimer
	
	// pulse - 
	public int d_pulse_interval = 5;
	public int d_pulse_reps = 5; 
	
	void Awake(){
		if(cellRule== null)
			Debug.LogWarning("No Rule selected, Please Select a Rule");
		if(!cellPrefab)
			Debug.LogWarning("No Cell Prefab Selected");
	}
	
	// ------ pass ---
	
	// Use this for initialization
	public void InstantiateCellArray () {
		InstantiateCellArray ( numberOfRows, numberOfColumns);
	}
	// overridden for manually setting settings
	public void InstantiateCellArray (int rows, int cols){	
		//initialize cell array
		cellArray = new List<Cell>(numberOfRows * numberOfColumns );
		
		// if this setting is enabled, use a random seed
		if(generateRandomSeed){
			randomSeed = Random.Range(0, int.MaxValue);
		}
		// assign the random number generator seed
		Random.seed = randomSeed;
		
		//?? this maybe shouldnt be coroutine
		StartCoroutine(RunStartup());
	}
	
	//
	// Calls the creation of the arry and also runs the debug test scripts
	//
	private IEnumerator RunStartup(){
		
		// Generate Cells
		Debug.Log(" Generating Cell Array ....");
		if(GenerateCells(numberOfRows, numberOfColumns)){
			Debug.Log("Cell Array Generated Successfully: " + numberOfRows + "x" + numberOfColumns + " Total Cells: " + cellArray.Count);
			DebugUtils.Assert(numberOfRows * numberOfColumns == cellArray.Count);
		}
		
		// run visual debugger
		if(debug_VisualTestCases){	
			Debug.Log("Running Visual Debugger");
			yield return StartCoroutine(Debug_VisualScan());
		}
		
		if(debug_TestEdgeCases){
			Debug.Log("Testing Edge Cases");
			yield return StartCoroutine(Debug_BoundaryConditions());
		}
		
		if(debug_VisualPulse){	
			Debug.Log("Running Visual Pulse - We Are Alive");
			yield return StartCoroutine(Debug_VisualPulse());
		}
	}		
		
	//
	public void StepUpdate(){
			
			// better ways to do this
			// co routines or a schedular
		
		// cellArray.StepUpdate();	
		
		// update all next states
		foreach(Cell c in cellArray){
			c.CacheNextUpdateState();
		}
		
		// apply all next states
		foreach(Cell c in cellArray){
			c.ApplyCachedUpdateState();
		}
		
		// apply effects dependent on state changes
		foreach(Cell c in cellArray){
			c.ApplyStateChangeEffects();
		}
		
	}
	
	// ******************* GRID LevEl Initialization Functions ************
	// Generate the grid
	// +++++++ 	
	private bool GenerateCells(int rows, int cols){
		
		if(cellPrefab == null){
			Debug.LogWarning("Cell Prefab null, instanting manual cell generation");
			
			GameObject go = new GameObject();
			go.AddComponent<Cell>();
			cellPrefab = go.transform;
		}
		
		bool successFlag = false;
		
		// check for
		// - -ve, 0 size, very large cases // upper maximum size
		// etc
		
		// hold things for a bit
		Transform tempTransform;
		Cell tempCell;
		
		for(int i=0; i < rows; i++){
			for(int j=0; j < cols; j++){
				// instantiate
				
				tempTransform = Instantiate(cellPrefab, transform.position + new Vector3(i, 0, j), Quaternion.identity) as Transform;
				
				// cehck for null object
				if(tempTransform != null){
					tempTransform.parent = this.transform;
					tempCell = tempTransform.GetComponent<Cell>();
					
					// check for cell
					if(tempCell != null){	
						// adds local knowledge to the cell
						
						int state = 0; // default state
						
						// check for random state init
						if(useRandomCellStates){
							if( Random.value < cellsStartedActive){
								state = 1;	
							}
						}
						
						// initialize cell (x, z, parent, starting state)
						tempCell.Init(i,j, this, state);
						
						// sets cell starting rule
						tempCell.SetRule(cellRule);
						
						tempCell.ApplyStateChangeEffects();
						
						// adds cell to the array
			
						cellArray.Add(tempCell);
						
						// if random, randomize
						
						
						successFlag = true;
					}else{
						Debug.LogError("Cell Prefab doesn't have a Cell Attached");	
					}
				}else{
					Debug.LogError("Cell Prefab Transform failed to Instantiate");	
				}
			}
		}
		return successFlag;
	}
	
//	// set random values for the grid
//	public void SetRandomValues(){
//		foreach(Cell c in cellArray){
//			if( Random.value < percentSeedStarting)
//				c.SetState(1);	
//			else 
//				c.SetState(0);	
//		}
//	}
	
	
	private void DestroyCells(){
		Debug.Log("Destroying Cells");
		// destroys all the cells, used ot call cleanup or save or whatever
		foreach(Cell c in cellArray){
			Destroy(c.gameObject);
		}
		
		cellArray = new List<Cell>();
	}
	

	// **************** Apply Rules ****************
	public void ApplySequentialScan(){
		
	}
	
	//apply Basic rule to all cells
	public void ApplyConwayBasicRule(){
		// check neighbors according to game of life rules
		
		// check neighbors state
//		int numNeighbors = NumNeighborsAlive();
		
	}
		
//************ Outside ( Public)  Utils************
// get enighbors by cell lookup
// default gets surrounding 8
//	public List<Cell> GetNeighborCells(Cell targetCell){
//		return GetNeighborCells(targetCell, 1);	
//	}
//	// overloaded // currently returns 
//	// range 0 = 4, 1 = 8, 2 = 12
//	public List<Cell> GetNeighborCells(Cell targetCell, int range){
//		
//		int targetCellIndex = cellArray.FindIndex(targetCell);
//		
//		if( targetCellIndex < 0 || targetCellIndex > cellArray.Count)
//			Debug.Log("Target Cell Index Out of Bounds : " + targetCellIndex);
//		
//		// add horizontal and vertical neighbors; 
//		return GetNeighborCellByIndex(targetCellIndex, range);
//		
//	}
	
	// *******************************************
	// eg used by cells, etc
	public List<Cell> GetNeighborCells(int row, int col, int range){
		List<Cell> returnList = new List<Cell>();

		// check for valid input
		if(range < 0 || range > 2){
			Debug.LogWarning("Range Invalid, resetting to 0");
			range = 0;
		}
		
		returnList.Add(GetCellWithWrap(row-1, col));
		returnList.Add(GetCellWithWrap(row+1, col));
		returnList.Add(GetCellWithWrap(row, col-1));
		returnList.Add(GetCellWithWrap(row, col+1));
	
		// diagonal 1 
		if(range> 0){
			returnList.Add(GetCellWithWrap(row + 1, col + 1));
			returnList.Add(GetCellWithWrap(row + 1, col - 1));
			returnList.Add(GetCellWithWrap(row - 1, col + 1));
			returnList.Add(GetCellWithWrap(row - 1, col - 1));
		}
		
		// horiz & vert 2 squares
		if( range > 1){
			returnList.Add(GetCellWithWrap(row + 2, col));
			returnList.Add(GetCellWithWrap(row - 2, col));
			returnList.Add(GetCellWithWrap(row, col + 2));
			returnList.Add(GetCellWithWrap(row, col - 2));	
		}
		
		return returnList;
	}
	
	private Cell GetCellWithWrap(int row, int col){
		Debug.Log("GetCell - Accepting row : " + row + " col: " + col);

		
		// get above
		if(row < 0){
			row = row + numberOfRows;	
		}
		
		if(row > numberOfRows){
			row = row - numberOfRows;
		}
		
		if(col < 0){
			col = col + numberOfColumns;	
		}
		
		if(col > numberOfColumns){
			col = col - numberOfColumns;	
		}
		
		Debug.Log("GetCell - Returning row : " + row + " col: " + col);
		return GetCell(row, col);
	}
	
	
	// *********** PRivate ********
	
	// DOESNT WRAP RIGHT NOW< this all needs to be pulled out into its own data structure
	
//	// this is where the actual list gets grabbed
//	private List<Cell> GetNeighborCellByIndex(int targetCellIndex, int range){
//		List<Cell> returnList = new List<Cell>();
//	
//		// check for valid input
//		if(range < 0 || range > 2){
//			Debug.LogWarning("Range Invalid, resetting to 0");
//			range = 0;
//		}
//		
//		if(numberOfRows < 3 || numberOfColumns < 3)
//			Debug.LogWarning(" Array is too small, may get repeating neighbors. Range 0 requires 3x3");
//		// add cells horizontally and vertically adjacent to target
//		
//		// grab vertical neighbors
//		// above
//		// if on the top line
//		if(targetCellIndex - numberOfColumns < 0)
//			Debug.LogWarning("Edge Case - Top");
//		else
//			returnList.Add(targetCellIndex - numberOfColumns);
//		
//		// below	
//		if(targetCellIndex + numberOfColumns > cellArray.Count)
//			Debug.LogWarning("Edge Case - Bottom");
//		else 
//			returnList.Add(targetCellIndex + numberOfColumns);
//		
//		// grab horizontal neighbors
//		if(targetCellIndex )
//		
//		returnList.Add(GetCell
//		
//	}
	
		// *************** Utilities ***************				
			

	// Grab Cell by row / col coordinates 2d array
	private Cell GetCell(int row, int col){
		// catch out of bounds
		return GetCell(GetIndex(row, col));
	}
		// array index as a 1d array
	private Cell GetCell(int arrayIndex){
		
		Cell tempCellB = null;	
		
		// catch out of bounds
		if(cellArray != null){
			if( arrayIndex >= 0 || arrayIndex < cellArray.Count){
///				Debug.Log("Grabbing cellArray[ArrayIndex]: " + arrayIndex );
				tempCellB = cellArray[arrayIndex];
//				Debug.Log("Grabbed cellArray[ArrayIndex]: " + arrayIndex );
			}else{
				Debug.LogWarning("array Index Out of Bounds:" + arrayIndex + ", blank cell returned");
				tempCellB = new Cell();
			}
			
		}else{
			Debug.LogWarning("cellArray not initialized");	
		}
		
		// check for null cell
		if(tempCellB == null){
			Debug.LogWarning("Cell returned was Null");
		}
			
		return tempCellB;				
	}
	
	
	// Converts the Row/Col index to the 1d array index
	// get position index at Row * Col
	private int GetIndex(int row, int col){	
		
		Debug.Log("ROW/COL: " + row + "/" + col);
		if(row >= numberOfRows)
			row = row % numberOfRows;
		if(row < 0)
			row = numberOfRows - 1 ; ////////// 
		
		if(col >= numberOfColumns)
			col = col % numberOfColumns;
		if(col < 0)
			col = numberOfColumns - 1; ////////
		
		int ind = row * numberOfColumns + col;
		
		Debug.Log("index returned " + ind);
		
		return ind;	
	}
			
		
	//*********** DEBUG ***************
		
	// DEBUG- Runs visual scans on the peripherial and other edge cases	
	IEnumerator Debug_BoundaryConditions(){
		
		Debug.Log("BoundaryConditions");
	
		//Initialization
		List<Cell> tempCells = new List<Cell>();
		
		// run boundary testing		
		// ================================================================ 4 neighbors
		// ====== Test Boundary Condition #A0 - (max, max) 4 neighbors
		tempCells = GetNeighborCells( (numberOfRows - 1)/2, (numberOfColumns - 1)/2, 0);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.red));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #A0 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2);
		
		// ================================================================
		// ====== Test Boundary Condition #A1 - (0,0) 4 Neighbors
		tempCells = GetNeighborCells( 0, 0, 0);
		
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer));
		}	
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #A1 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2);
		
		// ================================================================
		// ====== Test Boundary Condition #A2 - (max, max) 4 neighbors
		tempCells = GetNeighborCells( numberOfRows - 1, numberOfColumns - 1, 0);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.red));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #A2 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2f);
				
		// ================================================================ 8 neighbors - blue
		// ====== Test Boundary Condition #b0 - (center) 8 neighbors
		tempCells = GetNeighborCells( (numberOfRows - 1)/2, (numberOfColumns - 1)/2, 1);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.blue));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #b0 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2f);
		
		// ================================================================
		// ====== Test Boundary Condition #b1 - (0, max) 8 neighbors
		tempCells = GetNeighborCells( 0 , numberOfColumns - 1, 1);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.blue));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #b1 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2f);
		
		// ================================================================
		// ====== Test Boundary Condition #b2 - (max, 0) 8 neighbors
		tempCells = GetNeighborCells( numberOfRows - 1 , 0, 1);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.blue));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #b2 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2f);
		
		// ================================================================ 12 neighbors - green
		// ====== Test Boundary Condition #b0 - (center) 8 neighbors
		tempCells = GetNeighborCells( (numberOfRows - 1)/2, (numberOfColumns - 1)/2, 2);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.green));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #c0 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2f);
		
		// ================================================================		
		// ====== Test Boundary Condition #b1 - (0, max) 12 neighbors
		tempCells = GetNeighborCells( 0 , numberOfColumns - 1, 2);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.green));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #c1 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2f);
		
		// ================================================================ 	
		// ====== Test Boundary Condition #b2 - (max, 0) 12 neighbors
		tempCells = GetNeighborCells( numberOfRows - 1, 0, 2);
		foreach( Cell c in tempCells){
			StartCoroutine(ApplyEffect(c, d_fadeTimer, Color.green));
		}
		
		// --- pause for a second
		Debug.Log("CellArray - Boundary Condition #c2 Passed");
		yield return new WaitForSeconds(d_fadeTimer * 2f);
	}
	
	// DEBUG - 
	// runs a sequential visual sequence and perhaps a heartbeat to check the automata cells for errors visually
	// returns true if it passes
	IEnumerator Debug_VisualScan(){
		Debug.Log(" Begin Visual Scan ");
		foreach(Cell c in cellArray){
			StartCoroutine(ApplyEffect(c, d_fadeTimer));

			// will step frames even at 0
			yield return new WaitForSeconds(d_iterationTimer);
		}
		
		// xx just give some time for fade to finish - theres a better way to do this
		yield return new WaitForSeconds(d_fadeTimer * 2);
		Debug.Log(" End Visual Scan ");
	}
	
	// DEBUG - Heartbeart
	IEnumerator Debug_VisualPulse(){
		Debug.Log(" Begin Visual Pulse ");
		
		// number of beats - d pulse interval
		for(int i=0; i< d_pulse_reps; i++){
	//	foreach(Cell c in cellArray){
			for(int j=0; j < numberOfColumns; j++){
				int tempRow = numberOfRows/2;
			
				// add bump
				if(j % (d_pulse_interval - 1) == 0)
					tempRow--;
				else if(j % (d_pulse_interval) == 0)
					tempRow++;
					
				// get appripriate cell
				Cell c = GetCell(tempRow, j);
			
				// assign new color
				StartCoroutine(ApplyEffect(c, d_fadeTimer));
				// will step frames even at 0
				yield return new WaitForSeconds(d_iterationTimer);
			}
		}	
		
		// xx just give some time for fade to finish - theres a better way to do this
		yield return new WaitForSeconds(d_fadeTimer + 1f);
		Debug.Log(" End Visual Pulse ");
	}
	
	// DEBUG -apply debug effect (turns square red and fades back)
	private IEnumerator ApplyEffect(Cell c, float fadeTime){
		return ApplyEffect(c, fadeTime, Color.red);
	}
	private IEnumerator ApplyEffect(Cell c, float fadeTime, Color pickedColor){

//		Debug.Log("Begin Effect");
		// apply effect
		Color tempColor = c.GetComponent<Renderer>().material.color;		
			
		c.GetComponent<Renderer>().material.color = pickedColor;
		yield return new WaitForSeconds(fadeTime);
		c.GetComponent<Renderer>().material.color = tempColor;
//		Debug.Log("End Effect");

	}
		
}
