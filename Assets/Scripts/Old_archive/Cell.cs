using UnityEngine;
using System.Collections.Generic;

public class Cell : MonoBehaviour{
	
	// current state
	public int currentState = 0;
	
	private bool nextStateReady = false;
	
	// next state stored here so we don't duplicate the whole file structure + individual cells can iterate at different rates
	private int nextState = -1;
	
	
	// position in the grid
	private int x_position;
	private int z_position;
	private AutomataCellArray parentAutomata; // parent array reference
	
	// the rule in effect here
	private AutomataRule activeRule;
	
	// Use this for initialization
	void Start () {
		if(activeRule == null){
			Debug.LogWarning("Requires a rule to run");	
		}
	}
	// -------------------- This is the main update loop --------------------------
	// call this ot update
	public void StepUpdate(){
		
		// calls all updates 
		CacheNextUpdateState();
		ApplyCachedUpdateState();
		ApplyStateChangeEffects();
	}
	
	// 1------- 
	// a controlled update loop. iterated by Automata
	// Step 1 /3 of the update loop
	public void CacheNextUpdateState(){
		
		// grabs the neighboring cell and the current state and queries the rule for an updated state
		nextState = activeRule.CalcNextState( GetNeighborCells(), currentState );
		nextStateReady = true; // activate flag
		
	}
	
	// 2------
	// apply the changes cached in next state to current state - note this is seperated out from the 
	// Step 2/3 of hte update loop
	public void ApplyCachedUpdateState(){
		
		if(nextState == -1){
			Debug.LogWarning("State Changes being applied before state is updated");	
		}	
		
		currentState = nextState;
		
		//update flags
		nextState = -1; // dirty the nextstate, make sure this is updated
		nextStateReady = false;
		
	}
	
	// this is the actual state change effect - this is going to have to be interfaced depending on the characteristics of the Cell
	// could be updating animation, pathfinding, characteristics, whatever - 
	// step 3/3 of main update loop
	public void ApplyStateChangeEffects(){
	
		if(currentState == 0)
			this.GetComponent<Renderer>().material.color = Color.black;
		else 
			this.GetComponent<Renderer>().material.color = Color.white;
		
	}
	
	// -------------------- // -------------------- // -------------------- 
	// -- Initialization
	// initialize the cell
	public void Init(int x_pos, int z_pos, AutomataCellArray parentAutomata){
		Init(x_pos, z_pos, parentAutomata, 0); // default state = 0;
	}
	public void Init(int x_pos, int z_pos, AutomataCellArray parentAutomata, int state){
		x_position = x_pos;
		z_position = z_pos;
		
		this.parentAutomata = parentAutomata;
		
		currentState = state;
	}
	
	// -- State Accessors
	// get current state
	public int GetState(){
		return currentState;	
	}
	
	// set the rule (0/1)
	public void SetRule(AutomataRule rule){
		activeRule = rule;
	}
	
	// Grabs the neighboring cells to this one (adjacent based on neighbor rules in AutomataRule)
	// maybe an interface to allow cells to communicate in a limited matter 
	private List<Cell> GetNeighborCells(){
		// Grabs number of neighbors from rule
		int neighbors = 1;
		
		if(activeRule != null){
			neighbors = activeRule.GetNeighborType();
		}else{
			Debug.LogWarning("No Rule Found in Cell, using default settings");	
		}
		
		// handle this in th parent ? might be useful to check proximity at some point
		return parentAutomata.GetNeighborCells(x_position, z_position, neighbors);
	}
}
