﻿using UnityEngine;
using System.Collections;

public class SimpleInteraction : MonoBehaviour {

	public Camera mainCamera;
	public SpriteRenderer boardSprite;
	private Texture2D canvasTexture;

	private Vector2 pixelCoordinates = Vector2.zero;

	public Color clickColor = Color.red;

	// Use this for initialization
	void Start () {
		canvasTexture = boardSprite.sprite.texture;
		// [ ] error checking
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if ( Input.GetMouseButton(0) ){

			// color = new Color();
			pixelCoordinates = Vector2.zero;

			Vector2 mousePos = Input.mousePosition;
			Vector2 viewportPos = mainCamera.ScreenToViewportPoint(mousePos);

			if(viewportPos.x < 0.0f || viewportPos.x > 1.0f || viewportPos.y < 0.0f || viewportPos.y > 1.0f){ 

			}else{
				// Cast a ray
				Ray ray = mainCamera.ViewportPointToRay(viewportPos);
				
				// check for intersection
				if( IntersectsSprite(canvasTexture, ray)){
					
					// if we hit a pixel, get its color and update it to new Color
					Color c = canvasTexture.GetPixel((int)pixelCoordinates.x, (int)pixelCoordinates.y);
					
					canvasTexture.SetPixel((int)pixelCoordinates.x, (int)pixelCoordinates.y, clickColor );
					canvasTexture.Apply();
				}
			}

		}
	}

	private bool IntersectsSprite(Texture2D texture, Ray ray){

//		col = new Color();
//
//		if( spriteRenderer == null) return false;
//
//		Sprite sprite spriteRenderer.Sprite;
//
//		if( sprite == null) return false;

//		Texture2D tex = sprite.texture;
		if(texture == null) return false;

		// create a plane with the same orientation as the sprite
		Plane plane = new Plane(transform.forward, transform.position);

		// itersect ray and plane
		float rayIntersectDist; // distance from ray origin to the contact point
		if( !plane.Raycast(ray, out rayIntersectDist)) return false; // no intersection

		// convert world position to sprite position

		// world to localMatrix.MultiplyPoint3x4 returns a value based on the texture dimensions (+/- half texDimension / pixelsPerUnit) )
		// 0, 0 corresponds to the center of the sprite     
		Vector3 spritePos = boardSprite.worldToLocalMatrix.MultiplyPoint3x4(ray.origin + (ray.direction * rayIntersectDist));

		// NOT TESTED WITH ATLASES OR TIGHT MESHES
		// May need to use sprite.rect, sprite.textureRect, or sprite.textureRectOffset



		float pixelsPerUnit = boardSprite.sprite.pixelsPerUnit;
		float texWidth = boardSprite.sprite.textureRect.width;
		float texHeight = boardSprite.sprite.textureRect.height;
		
		float halfTexWidth = texWidth * 0.5f;
		float halfTexHeight = texHeight * 0.5f;
		
		// Convert to pixel position, offsetting so 0,0 is in lower left instead of center
		int texPosX = (int)(spritePos.x * pixelsPerUnit + halfTexWidth);
		int texPosY = (int)(spritePos.y * pixelsPerUnit + halfTexHeight);
		
		// Check if pixel is within texture
		if(texPosX < 0 || texPosX >= texWidth) return false; // out of bounds
		if(texPosY < 0 || texPosY >= texHeight) return false; // out of bounds

		pixelCoordinates.x = texPosX;
		pixelCoordinates.y = texPosY;

		// Get pixel color
		//color = texture.GetPixel(texPosX, texPosY);
		
		return true;
	}
}
