using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rule_GameOfLife : AutomataRule {
	
	// will survive in these conditions
	public int[] inhabitedNumbers = new int[] {2,3};
	public int[] uninhabitedNumbers = new int[] {3};
	
	public override int CalcNextState (List<Cell> neighborCells, int currentState)
	{
		int activeCellCount = 0;
		int returnState = -1;
		
		// count living neighbors
		foreach(Cell c in neighborCells){
			// if neighbor is alive
			if( c.GetState() == 1){
				// count it
				activeCellCount++;	
			}
		}
		
		// if alive
		if(currentState == 1){
			foreach(int surviveNumber in inhabitedNumbers){
				if(surviveNumber == activeCellCount){
					// rule succeeds and cell survives
					returnState = 1;
					break;		
				}else{
					// dies
					returnState = 0;	
				}
			}
			// cell is dead
		}else if(currentState == 0){
			foreach(int surviveNumber in uninhabitedNumbers){
				if(surviveNumber == activeCellCount){
					// rule succeeds and cell dies
					returnState = 1;
					break;		
				}else{
					returnState = 0;	
				}
			}	
		}else {
			Debug.LogWarning("UNINSTANTITED STATE CALLED");
			returnState = -1;
		}
		
		return returnState;
	//	throw new System.NotImplementedException ();
	}
}
