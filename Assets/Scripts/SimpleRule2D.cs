using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleRule2D:MonoBehaviour{
	
	public bool active = false;


	// currently both rulesets are redundant and bothupdate the same way,
	// [ ] Refactor Cell Array to use this rule instead
	public int[] baseTwoRule = new int[]{0,0,0,0,0,0,0,0,0};
	// in order 0 = all white, center is white
	//			1 = all white surrounding, center is black 
	// 			2 = one surrounding
	//          3 = 
//	public bool twoD_whiteNoNeighbors = false;
//	public bool twoD_blackNoNeighbors = false;
	// eg [0] = if no neighbors, [1] one neighbor, etc
	public bool[] twoD_numberOfNeighbors = new bool[]{false,false,false,false,false,false,false,false, false};

	// translated in to an int in the cell, maybe standardize
//	public enum NumberOfNeighbors{
//		fourNeighbors,
//		eightneighbors,
//		twelveNeighbors
//	}
	
	//public NumberOfNeighbors neighbors = NumberOfNeighbors.eightneighbors; 

	// cells are passed in as center, top center, top right, then continuing clockwise
	public Color CalcTwoDState( Color[] activeCells){

		int selectedRule = -1;
		Color returnColor = Color.red;

		int otherCellsCounter = 0;

		for(int i = 1; i < activeCells.Length; i ++){
			if( activeCells[i].r < 0.5f){
				otherCellsCounter++;
			}
		}

//		if( otherCellsCounter == 0){
//			if( activeCells[0].r < 0.5f ){	
//				if(twoD_blackNoNeighbors){
//					returnColor = Color.black;
//				}else{
//					returnColor = Color.white;
//				}
//			}else{
//				if(twoD_whiteNoNeighbors){
//					returnColor = Color.white;
//				}else{
//					returnColor = Color.black;
//				}
//			}
//		}else{

			if(twoD_numberOfNeighbors[otherCellsCounter]){
				returnColor = Color.black;
			}else{
				returnColor = Color.white;
			}

//		}

		return returnColor;
	}

	public string ToString(){
		// [ ] Calculate the base 10 from the base two, or whatever encoding we're using

		return "rule" + GetBaseTenRule();
	}

	// [ ] confirm algorithm
	public int GetBaseTenRule(){
		int counter = 0;
	
		// iterate and convert to base ten
		for(int i=0; i< baseTwoRule.Length; i++){
			if( baseTwoRule[i] == 1){
				counter = counter + (int) Mathf.Pow(2, i);
			}
		}
		Debug.Log ("Calced " + counter + " current rule");
		return counter;
	}

	public void SetRuleByBaseTen(int baseTenDigit){

		int remainder = 0;

		for( int i=0; i < baseTwoRule.Length; i++){

			// check for zero
			if( baseTenDigit == 0){
				baseTwoRule[i] = 0;
				twoD_numberOfNeighbors[i]= false;
			}else{

				remainder = baseTenDigit % 2;
				baseTenDigit = baseTenDigit / 2;
				baseTwoRule[i] = remainder;
				if(remainder == 1)
					twoD_numberOfNeighbors[i] = true;
				else 
					twoD_numberOfNeighbors[i] = false;
			}
		}

		// update bool rule

	}

	public void DisableFlicker(){
		twoD_numberOfNeighbors[0] = false;
	}
}