using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleRule1D:MonoBehaviour{

	public bool oneD = false;
	public bool twoD = false;


//	public int base10Rule = 

	// true is black
	public bool[] oneDRule = new bool[]{false, false, false, false, false, false, false, false};


	// in order 0 = all white, center is white
	//			1 = all white surrounding, center is black 
	// 			2 = one surrounding
	//          3 = 
	public bool twoD_whiteNoNeighbors = false;
	public bool twoD_blackNoNeighbors = false;
	// eg [0] = if no neighbors, [1] one neighbor, etc
	public bool[] twoD_numberOfNeighbors = new bool[]{false,false,false,false,false,false,false,false, false};

	// translated in to an int in the cell, maybe standardize
//	public enum NumberOfNeighbors{
//		fourNeighbors,
//		eightneighbors,
//		twelveNeighbors
//	}
	
	//public NumberOfNeighbors neighbors = NumberOfNeighbors.eightneighbors; 
	
	public int base10toBase2(int base10){
		int base2;



		return 0;
	}

	// cells are passed in as center, top center, top right, then continuing clockwise
	public Color CalcTwoDState( Color[] activeCells){

		int selectedRule = -1;
		Color returnColor = Color.red;

		int otherCellsCounter = 0;

		for(int i = 1; i < activeCells.Length; i ++){
			if( activeCells[i].r < 0.5f){
				otherCellsCounter++;
			}
		}

		if( otherCellsCounter == 0){
			if( activeCells[0].r < 0.5f ){	
				if(twoD_blackNoNeighbors){
					returnColor = Color.black;
				}else{
					returnColor = Color.white;
				}
			}else{
				if(twoD_whiteNoNeighbors){
					returnColor = Color.white;
				}else{
					returnColor = Color.black;
				}
			}
		}else{

			if(twoD_numberOfNeighbors[otherCellsCounter]){
				returnColor = Color.black;
			}else{
				returnColor = Color.white;
			}

		}

		return returnColor;
	}

	public Color CalcOneDState(Color upLeft, Color upCenter, Color upRight){

		int selectedRule = -1;

		Color tempColor = Color.white;


		if( upLeft.r < 0.5f){
			// first 4
			if( upCenter.r < 0.5f){
				// first 2
				if(upRight.r < 0.5f){ 
					selectedRule = 0;
				}else{
					selectedRule = 1;
				}
			}else{
				// 3 & 4
				if(upRight.r < 0.5f){ 
					selectedRule = 2;
				}else{
					selectedRule = 3;
				}
			}
		}else{
					// second 4
			if( upCenter.r < 0.5f){
				// first 2
				if(upRight.r < 0.5f){ 
					selectedRule = 4;
				}else{
					selectedRule = 5;
				}
			}else{
				// 3 & 4
				if(upRight.r < 0.5f){ 
					selectedRule = 6;
				}else{
					selectedRule = 7;
				}
			}
		}


		if(oneDRule[selectedRule]){
			tempColor = Color.black;
		}else{
			tempColor = Color.white;
		}
	
//		if( rule == 0){
//			if( rule[0] ){
//				tempColor = Color.black;
//			}else{
//				tempColor = Color.white;
//			}
//		}else if( rule == 1){ 
//
//		}else if( rule == 2){
//		
//		}else if( rule == 3){
//
//		}else if( rule == 4){
//		
//		}else if( rule == 5){
//		
//		}else if( rule == 6){
//		
//		}else if( rule == 7){
//		
//		}else {
//			Debug.Log ("Illigal Rule Selected");
//		}

		return tempColor;
	}
	
	// returns the neighbors code from the enum
//	public virtual int GetNeighborType(){
//		return (int)neighbors;
//	}
}
