﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// A structure for holding a set of rules
public class SimpleRuleset : MonoBehaviour {

	public List<int> ruleset;
	public int lastRule = 0 ;

	// Use this for initialization
	void Start () {
		ruleset = new List<int>();
	}
	
	// add
	public void Add(int rule){
		ruleset.Add (rule);
	}

	// remove
//	public void RemoveRule(int rule){
//
//	}

	// get by id
	public int GetRule(int id){
		if(id > 0 && id < ruleset.Count){
			lastRule = id;
			return ruleset[id];
		}else{
			Debug.LogWarning ("out of bounds rule id");
			return 0;
		}
	}
	public int GetNextRule(){
		lastRule++;
		if(lastRule >= ruleset.Count){
			lastRule = 0;
		}

		return ruleset[lastRule];
	}

	// get random
	public int GetRandomRule(){
		return Random.Range(0, ruleset.Count);
	}
}
