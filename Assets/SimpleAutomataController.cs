using UnityEngine;
using System.Collections;

public class SimpleAutomataController : MonoBehaviour {

	public SimpleCellArray cellArray;
	private Color avgColor = Color.grey;

	public SimpleRuleset whitePositiveRules;
	public SimpleRuleset equilibriumRules;
	public SimpleRuleset blackPositiveRules;
	public SimpleRuleset interestingRules;

	public float upperThreshold = .75f;
	public float lowerThreshold = .25f;

	public float timer = 0f;
	public float timestep = 2.0f; // every two seconds change the rule

	public bool mode_automatedMode = false;

	public bool option_filterFlickering = false;
	public bool option_startWithRandomInteresting = false;

	// Use this for initialization
	void Start () {
		cellArray = GetComponent<SimpleCellArray>();
		cellArray.cellRule.SetRuleByBaseTen( interestingRules.GetRandomRule());
	}
	
	// Update is called once per frame
	void Update () {

		avgColor = cellArray.GetAverageColor();

		float currentSumDarkness = (avgColor.r + avgColor.g + avgColor.b) / 3 ;

		if(Input.GetAxis("Horizontal") < -0.5f){
			// left
			cellArray.cellRule.SetRuleByBaseTen( interestingRules.GetRandomRule() );

		}else if (Input.GetAxis("Horizontal") > 0.5f){
			// right
			cellArray.cellRule.SetRuleByBaseTen( equilibriumRules.GetNextRule() );

		}else if (Input.GetAxis("Vertical") < -0.5f){
			// down
			cellArray.cellRule.SetRuleByBaseTen( blackPositiveRules.GetNextRule() );


		}else if (Input.GetAxis("Vertical") > 0.5f){
			// up
			cellArray.cellRule.SetRuleByBaseTen( whitePositiveRules.GetNextRule());
		}

		if(Input.GetButtonDown ("ToggleAutomatedMode")){
			mode_automatedMode = !mode_automatedMode;
		}

		if(Input.GetButtonDown ("IncreaseAutomatedTimestep")){

			timestep = timestep * 1.5f;
			if(timestep < 0.01f){
				timestep = 0.01f;
			}
		}else if(Input.GetButtonDown ("DecreaseAutomatedTimestep")){
			if(timestep < 0.01f){
				timestep = 0f;
			}
			timestep = timestep * 0.75f;
		}

		if ( Input.GetButtonDown ("FilterFlickering")){
			option_filterFlickering = !option_filterFlickering;
		}

		if(mode_automatedMode){

			if( currentSumDarkness < lowerThreshold){
				cellArray.cellRule.SetRuleByBaseTen( whitePositiveRules.GetRandomRule());
			}else if( currentSumDarkness > upperThreshold){
				cellArray.cellRule.SetRuleByBaseTen( blackPositiveRules.GetRandomRule());
			}

			if(timer < Time.time){
				timer = Time.time + timestep;
				cellArray.cellRule.SetRuleByBaseTen( interestingRules.GetRandomRule());
			}
		}	

		if( option_filterFlickering ){
			cellArray.cellRule.DisableFlicker();
		}
	}
}
