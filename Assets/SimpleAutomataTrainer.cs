﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleAutomataTrainer : MonoBehaviour {

	public bool isControllingSequence = false;

	public SimpleRule2D ruleTemplate;
	public SimpleCellArray cellArray;

	private int counter = 0;

	// number of rounds it must survive to qualify
	public int minimumThresholdIteration = 100;
	// max number of rounds it must survive
	public int iterations = 1000;

	public int currentRule = 0;
	public int MaxRule = 511;

	// outside of this boundary is destroyed and reset
	public float upperThreshold = .75f;
	public float lowerThreshold = .25f;

	public SimpleRuleset successfulRuleList;
	public SimpleRuleset fadeToBlackRuleList;
	public SimpleRuleset fadeToWhiteRuleList;
	public SimpleRuleset stayedPrettyMuchInTheMiddleRuleList;
	public SimpleRuleset interestingRules;
	Color avgColor = Color.grey;

	public Color cachedColor = Color.white;
	public bool option_filterFlickering = true;


	private float currentSumDarkness = 0f;

	// Use this for initialization
	void Start () {
	//	NextRule ();
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetButtonDown("Jump")){
			interestingRules.Add (currentRule);
		}

		if(isControllingSequence){

			RunTrainer();
		}

	}

	private void RunTrainer(){

		
		bool flag = true;
		
		avgColor = cellArray.GetAverageColor();
		
		if(option_filterFlickering ){
			
			if( Mathf.Abs( cachedColor.r - avgColor.r) > 0.8f)
				flag = false;
		}
		
		cachedColor = avgColor;
		
		if( counter < iterations && avgColor != Color.white && avgColor != Color.black && flag){
			
			//?
			currentSumDarkness = (avgColor.r + avgColor.g + avgColor.b) / 3 ;
			
			// [ ] Move this & fix 
			
			
		}else{
			if( counter > minimumThresholdIteration){
				// rule success
				successfulRuleList.Add (currentRule);
				
				// black or white
				// if color tends towards white
				if( currentSumDarkness < lowerThreshold){
					//	cellArray.cellRule = whitePositiveRules;
					fadeToWhiteRuleList.Add( currentRule );
					
					// if color tends towards black
				}else if( currentSumDarkness > upperThreshold){
					//	cellArray.cellRule = blackPositiveRules;
					fadeToBlackRuleList.Add ( currentRule);
					
				}else{
					stayedPrettyMuchInTheMiddleRuleList.Add( currentRule);
				}
				
				NextRule ();
			}
		}
		counter++;

	}

	private void NextRule(){

		currentRule++;
		ruleTemplate.SetRuleByBaseTen(currentRule);
		cellArray.FullReset(ruleTemplate);
		counter = 0;
	}
}
